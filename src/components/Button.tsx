"use client"
import styles from '@/components/Button.module.scss';

export default function Button({children, onClick, disabled}: ButtonProps) {
  return <button className={`${styles.colorfulButton} flex font-bold rounded-full px-11 py-6 mb-16`} disabled={disabled} onClick={onClick}>{children}</button>
}

type ButtonProps = {
  children: React.ReactNode
  onClick?: () => void
  disabled?: boolean
}