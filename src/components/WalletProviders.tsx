"use client"
import { useEffect, useState } from 'react';
import { getDefaultWallets, RainbowKitProvider } from "@rainbow-me/rainbowkit";
import { type Chain, configureChains, createClient, WagmiConfig } from "wagmi";
import { jsonRpcProvider } from "wagmi/providers/jsonRpc";
import { publicProvider } from "wagmi/providers/public";
import { sepolia, hardhat } from 'wagmi/chains';

const alchemyAPIKey = process.env.NEXT_PUBLIC_ALCHMEY_SEPOLIA_KEY as string

const envChain: Chain = process.env.NEXT_PUBLIC_NETWORK_NAME === sepolia.name ? sepolia : hardhat
const envProviders = envChain === sepolia ?
  [
    jsonRpcProvider({rpc: (chain: Chain) => ({
      http: `https://eth-sepolia.g.alchemy.com/v2/${alchemyAPIKey}`,
      webSocket: `wss://eth-sepolia.g.alchemy.com/v2/${alchemyAPIKey}`
    })}),
    jsonRpcProvider({rpc: (chain: Chain) => ({
      http: "https://ethereum-sepolia.publicnode.com",
      webSocket: "wss://ethereum-sepolia.publicnode.com"
    })}),
    publicProvider(),
  ] :
  [
    jsonRpcProvider({rpc: (chain: Chain) => ({http: "http://localhost:8545" })})
  ]

const { chains, provider } = configureChains([envChain], envProviders);

const appName = process.env.NEXT_PUBLIC_WALLETCONNECT_APP_NAME as string
const projectId = process.env.NEXT_PUBLIC_WALLETCONNECT_PROJECT_ID as string
const { connectors } = getDefaultWallets({ appName, projectId, chains });
const client = createClient({ autoConnect: true, provider, connectors });

export default function WalletProviders({ children }: { children: React.ReactNode}) {
  // This is a hackish way to prevent SSR from producing so many hydration errors
  const [isClient, setIsClient] = useState(false)
  useEffect(() => {
  setIsClient(true)
  }, [])
  if (!isClient) return null;

  return (
    <WagmiConfig client={client}>
      <RainbowKitProvider chains={chains}>
        {children}
      </RainbowKitProvider>
    </WagmiConfig>
  );
}
