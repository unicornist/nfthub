"use client"
import { useState } from 'react';
import Image from "next/image";
import Link from "next/link";
import { faDiscord, faInstagram, faTwitter } from '@fortawesome/free-brands-svg-icons';
import { faBars, faWallet } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useConnectModal, useAccountModal, useChainModal } from '@rainbow-me/rainbowkit';

export default function Header() {
  const [open, setOpen] = useState<boolean>(false);
  const openSidebar = () => {
    setOpen(true)
    document.body.style.overflow = 'hidden';
  }
  const closeSidebar = () => {
    setOpen(false)
    document.body.style.overflow = 'unset';
  }

  const { openConnectModal } = useConnectModal();
  const { openAccountModal } = useAccountModal();
  const { openChainModal } = useChainModal();

  const handleWalletClick = (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
    e.preventDefault();
    if (openAccountModal) {
      openAccountModal()
    } else if (openConnectModal) {
      openConnectModal()
    } else if (openChainModal) {
      openChainModal()
    }
  }

  return (
    <>
      <div className='flex justify-between w-full items-center mt-8'>
        <Image src="/logo.svg" width="145" height="40" alt="logo" />

        <ul className='hidden lg:flex'>
          <li><Link href="" className='py-8 px-4 text-lg font-bold'>Home</Link></li>
          <li><Link href="" className='py-8 px-4 text-lg font-bold'>Collection</Link></li>
          <li><Link href="" className='py-8 px-4 text-lg font-bold'>Choose</Link></li>
          <li><Link href="" className='py-8 px-4 text-lg font-bold'>About</Link></li>
          <li><Link href="" className='py-8 px-4 text-lg font-bold'>Roadmap</Link></li>
          <li><Link href="" className='py-8 px-4 text-lg font-bold'>Blog</Link></li>
        </ul>

        <div className='gap-5 items-center hidden lg:flex'>
          <a href="#" className='text-sm w-6'><FontAwesomeIcon size="xl" icon={faTwitter} /></a>
          <a href="#" className='text-sm w-6'><FontAwesomeIcon size="xl" icon={faDiscord} /></a>
          <a href="#" className='text-sm w-6'><FontAwesomeIcon size="xl" icon={faInstagram} /></a>
          <a href="#" className='text-sm w-6' onClick={handleWalletClick}><FontAwesomeIcon size="xl" icon={faWallet} /></a>
        </div>

        <button className="text-sm w-6 flex lg:hidden" onClick={openSidebar}>
          <FontAwesomeIcon icon={faBars} />
        </button>
      </div>

      <div className={`${open ? 'opacity-100 visible' : 'opacity-0 invisible'} fixed top-0 left-0 w-full h-full bg-black bg-opacity-50 cursor-pointer transition-all`} onClick={closeSidebar}></div>
      <div className={`${open ? 'opacity-100 visible' : 'opacity-0 invisible'} absolute w-72 h-full bg-slate-700 right-0 top-0 p-10 z-10 transition-all`}>
        <ul className='mb-20'>
          <li className='my-5'><Link href="" className='py-8 px-4'>Home</Link></li>
          <li className='my-5'><Link href="" className='py-8 px-4'>Collection</Link></li>
          <li className='my-5'><Link href="" className='py-8 px-4'>Choose</Link></li>
          <li className='my-5'><Link href="" className='py-8 px-4'>About</Link></li>
          <li className='my-5'><Link href="" className='py-8 px-4'>Roadmap</Link></li>
          <li className='my-5'><Link href="" className='py-8 px-4'>Blog</Link></li>
        </ul>

        <div className='flex justify-around'>
          <a href="#" className='text-sm w-6'><FontAwesomeIcon size="xl" icon={faTwitter} /></a>
          <a href="#" className='text-sm w-6'><FontAwesomeIcon size="xl" icon={faDiscord} /></a>
          <a href="#" className='text-sm w-6'><FontAwesomeIcon size="xl" icon={faInstagram} /></a>
          <a href="#" className='text-sm w-6' onClick={handleWalletClick}><FontAwesomeIcon size="xl" icon={faWallet} /></a>
        </div>
      </div>
    </>
  )
}
