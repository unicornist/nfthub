"use client"
import styles from '@/components/NFTCard.module.scss';
import Image from "next/image";

const mockNFTs = [
  { by: 'TheSalvare', imageUrl: '/nft1.jpeg' },
  { by: 'TheNative', imageUrl: '/nft2.jpeg' },
  { by: 'GeorgZvic', imageUrl: '/nft3.jpeg' },
  { by: 'YazoiLup', imageUrl: '/nft4.jpeg' }
]

export default function NFTCard({tokenId, collectionName = ''}: {tokenId: string, collectionName?: string}) {
  const mockData = mockNFTs[+tokenId % 4]
  return (
    <div>
      <div className="rounded-2xl overflow-hidden">
        <Image src={mockData.imageUrl} width="600" height="800" alt="nft image" className="mx-auto" />
      </div>
      <div className="p-6">
        <span className="block font-bold text-2xl mb-3">{collectionName} #{tokenId}</span>
        <span className={`${styles.minter} leading-6`}>By {mockData.by}</span>
      </div>
    </div>
  )
}
