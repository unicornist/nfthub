import { useAccount, useContractRead, usePrepareContractWrite, useContractWrite, useWaitForTransaction } from 'wagmi'
import { BigNumber } from 'ethers'
import abi from '@/hooks/NFT.json'
const address = process.env.NEXT_PUBLIC_NFT_ADDRESS as `0x${string}`

export default function useNFTContract({ onMintSignError, onMintConfirmed }: useNFTContractOptions) {
    const { address: userAddress } = useAccount()

    const { data: collectionName } = useContractRead({address, abi, functionName: 'name'})
    const { data: myNFTs, isError: myNFTsError, isLoading: myNFTsLoading } = useContractRead({address, abi, functionName: 'tokensOfOwner', args: [userAddress], watch: true})

    const { config } = usePrepareContractWrite({ address, abi, functionName: 'mint', args: [1] });
    const { data: mintHashData, isLoading: mintSigning, error: mintSigningError, write: mint } = useContractWrite({
        ...config,
        onError: onMintSignError,
    });

    const { isSuccess: mintConfirmed, isLoading: mintConfirming } = useWaitForTransaction({
        hash: mintHashData?.hash,
        onSuccess: onMintConfirmed
    })
  
    return {
        collectionName: collectionName as string | undefined,

        myNFTs: myNFTs as Array<BigNumber> | undefined,
        myNFTsError,
        myNFTsLoading,
        
        mint,
        mintSigning,
        mintSigningError,
        mintConfirming,
        mintConfirmed,
    }
}

export interface useNFTContractOptions {
    onMintConfirmed: () => void
    onMintSignError: (error: Error) => void
}