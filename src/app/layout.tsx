import type { Metadata } from 'next'
import { Inter } from 'next/font/google'

import "@rainbow-me/rainbowkit/styles.css";
import './globals.css'

import Header from "@/components/Header";
import WalletProviders from "@/components/WalletProviders";
import { Toaster } from 'react-hot-toast';

const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: 'NFTHub',
  description: 'NFT Hub by Babak',
}

export default function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html lang="en" suppressHydrationWarning>
      <body className={inter.className} suppressHydrationWarning>
        <main className="max-w-7xl mx-auto px-5">
          <WalletProviders>
            <Header />
            {children}
          </WalletProviders>
          <Toaster position="bottom-center" />
        </main>
      </body>
    </html>
  )
}
