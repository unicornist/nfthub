"use client"
import { useCallback, useRef } from 'react';
import styles from '@/app/home.module.scss';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Image from "next/image";
import { useConnectModal, useChainModal } from '@rainbow-me/rainbowkit';
import toast from 'react-hot-toast';
import useNFTContract from "@/hooks/useNFTContract";
import NFTCard from '@/components/NFTCard';
import Spinner from '@/components/Spinner';
import Button from '@/components/Button';
import { useNetwork } from 'wagmi';

export default function Home() {
  const yourNFTsHeader = useRef<HTMLSpanElement>(null)

  const { openConnectModal } = useConnectModal()
  const { openChainModal } = useChainModal();
  const { chain, chains } = useNetwork()
  const inCorrectChain = chain?.id === chains[0]?.id

  const {
    collectionName,
    myNFTsLoading,
    myNFTsError,
    myNFTs,
    mint,
    mintSigning,
    mintConfirming,
  } = useNFTContract({
    onMintSignError: () => {
      toast.error(<>You&apos;ve rejected the transaction.</>)
    },
    onMintConfirmed: () => {
      toast.success(<>NFT Minted!</>)
      yourNFTsHeader.current?.scrollIntoView({ behavior: 'smooth' })
    }
  })

  return (<>
    <div className="mt-20 mb-32 lg:mt-40 lg:mb-56 grid lg:grid-cols-2 items-center">
      <div className="lg:pr-16 mb-10 lg:mb-0">
        <h1 className="text-5xl lg:text-7xl font-bold mb-6">High Quality NFT Collection</h1>
        <p className={`${styles.heroText} mb-8`}>
          A 890 piece custom Nerko&apos;s collection is joining the NFT space on Opensea.
        </p>

        { openConnectModal &&
          <Button onClick={openConnectModal}>CONNECT WALLET <FontAwesomeIcon icon={faArrowRight} className="w-6 -rotate-45" /></Button>
        }
        { !inCorrectChain &&
          <Button onClick={openChainModal}>SWITCH NETWORK <FontAwesomeIcon icon={faArrowRight} className="w-6 -rotate-45" /></Button>
        }
        { inCorrectChain && !mintSigning && !mintConfirming &&
          <Button onClick={mint} disabled={!mint}>MINT <FontAwesomeIcon icon={faArrowRight} className="w-6 -rotate-45" /></Button>
        }
        { mintSigning &&
          <Button disabled>Confirm Minting... <FontAwesomeIcon icon={faArrowRight} className="w-6 -rotate-45" /> </Button>
        }
        { mintConfirming &&
          <Button disabled><Spinner /> Minting...</Button>
        }

        <span className="text-4xl block">47k+</span>
        <span className="text-xd">Community members</span>
      </div>
      <div className="">
        <Image src="/hero-banner.png" width="1301" height="1278" alt="hero image" />
      </div>
    </div>

    <div className="flex flex-col lg:flex-row max-w-5xl justify-between items-center mx-auto mb-32 gap-8 lg:gap-0">
      <Image src="/metamask-logo.svg" width="176" height="35" alt="metamask logo" />
      <Image src="/bitgo-logo.svg" width="114" height="29" alt="bitgo logo" />
      <Image src="/coinbase-logo.svg" width="176" height="35" alt="coinbase logo" />
      <Image src="/trustwallet-logo.svg" width="176" height="35" alt="trustwallet logo" />
      <Image src="/exodus-logo.svg" width="176" height="35" alt="exodus logo" />
    </div>

    <div className="text-center">
      <Image src="/divider-vector.svg" width="80" height="19" alt="exodus logo" className="mx-auto mb-32" />

      <span ref={yourNFTsHeader} className={`${styles.heading} text-6xl font-bold mb-20 block`}>Your <span>nft’s</span></span>

      {openConnectModal && <p className={`${styles.heroText} my-8 mx-auto`}>
        Connect your wallet to see your NFTs.
      </p>}
      {myNFTsError && !openConnectModal && <p className={`${styles.heroText} my-8 mx-auto text-yellow-400`}>
        Could not list your NFTs.
      </p>}
      {myNFTsLoading && <p className={`${styles.heroText} my-8 mx-auto`}>
      <Spinner /> Loading your NFTs...
      </p>}
      {myNFTs && myNFTs.length === 0 && <p className={`${styles.heroText} my-8 mx-auto`}>
        You don&apos;t have any NFTs yet. <br /> Mint one now!
      </p>}
      {myNFTs && myNFTs.length > 0 &&
        <div className="grid md:grid-cols-2 lg:grid-cols-4 gap-12 mb-16">
          {myNFTs.map((item, i) => <NFTCard key={i} tokenId={String(item)} collectionName={collectionName} />)}
        </div>
      }

      <div className='flex justify-center'>
        <Button>View collection <FontAwesomeIcon icon={faArrowRight} className="w-6 -rotate-45" /></Button>
      </div>
    </div>
  </>)
}
